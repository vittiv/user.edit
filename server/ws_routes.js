/**
 * Main application web socket routes
 */

'use strict';

module.exports = function(socketio) {
  socketio.on('connection', function (socket) {  
        
    socket.on('user.update', function (data){
      require('./ws_controllers/user.controller').chenge(socket, socketio, data);
    });
    
    
  });
};
