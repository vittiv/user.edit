'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');

var InviteSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  inviteId: String
});

/**
 * Pre-save hook
 */
InviteSchema.pre('save', function( next) {
  this.inviteId = crypto.randomBytes(16).toString('hex');
  next();
});
  
module.exports = mongoose.model('Invite', InviteSchema);
