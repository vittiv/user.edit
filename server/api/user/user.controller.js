'use strict';

var User = require('./user.model');
var UserEditor = require('./user.editor.model');
var Invite = require("./invite.model");
var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var async = require('async');

var validationError = function(res, err) {
  return res.json(422, err);
};

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function(req, res) {
  User.find({}, '-salt -hashedPassword', function (err, users) {
    if(err) return res.send(500, err);
    res.json(200, users);
  });
};


/**
 * edit user
 */
exports.edit = function (req, res, next) {
  var userId = req.params.id;
  User.findById(userId, function (err, user) {
    if (err) return next(err);
    if (!user) return res.send(401);
    user.save(function(err, user) {
      if (err) return validationError(res, err);
      var token = jwt.sign({_id: user._id }, config.secrets.session, { expiresInMinutes: 60*5 });
      res.json({ token: token });
    });
  });
};

/**
 * Creates a new user
 */
exports.create = function (req, res, next) {
  Invite.findOne({inviteId: req.body.invite}, function(err, invite) { 
    var newUser = new User(req.body);
    newUser.provider = 'local';
    newUser.role = 'user';
    newUser.save(function(err, user) {
      if (err) return validationError(res, err);
      var token = jwt.sign({_id: user._id }, config.secrets.session, { expiresInMinutes: 60*5 });
      if(invite){ 
        var relation = new UserEditor();
        relation.user = invite.user;
        relation.editor = user;
        relation.save(function(err) {
          if (err) next(err);
        });
        Invite.findByIdAndRemove(invite._id, function(err, user) {
          if(err) return res.send(500, err);
        });
      }
      res.json({ token: token });
    });
  }).populate('user');
};

/**
 * Get a single user
 */
exports.show = function (req, res, next) {
  var userId = req.params.id;

  User.findById(userId, function (err, user) {
    if (err) return next(err);
    if (!user) return res.send(401);
    res.json(user.profile);
  });
};

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function(req, res) {
  User.findByIdAndRemove(req.params.id, function(err, user) {
    if(err) return res.send(500, err);
    return res.send(204);
  });
};

/**
 * Change a users password
 */
exports.changePassword = function(req, res, next) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  User.findById(userId, function (err, user) {
    if(user.authenticate(oldPass)) {
      user.password = newPass;
      user.save(function(err) {
        if (err) return validationError(res, err);
        res.send(200);
      });
    } else {
      res.send(403);
    }
  });
};

/**
 * Get my info
 */
exports.me = function(req, res, next) {
  var userId = req.user._id;
  User.findOne({
    _id: userId
  }, '-salt -hashedPassword', function(err, user) { // don't ever give out the password or salt
    if (err) return next(err);
    if (!user) return res.json(401);
    res.json(user);
  });  
};

/**
 * Authentication callback
 */
exports.authCallback = function(req, res, next) {
  res.redirect('/');
};


/**
 * Get editable Users
 */
exports.getEditableUsers = function(req, res, next) {
  UserEditor.find({editor: req.params.id}, function(err, users) { 
    if (err) return next(err);
    if (!users) return res.json(401);
    async.each(users, function(el, next) {       
      UserEditor.find({user: el.user._id}, function(err, editors) { 
        if (err) return next(err);
        if (!editors) return res.json(401);
        el.user.editors = editors;
        next();
      }).populate('editor');  
    }, function(err) {
      if (err) {
        return handleError(res, err);
      }
      res.json(200, {status: 'success', data: users});
    });
  }).populate('user');  
};

/**
 * 
 */
var sendEmail = function(params) {
  var email   = require("emailjs");
  var server  = email.server.connect(config.smtp);
  server.send(params, function(err, message) {
    console.log(err || message); 
  });
};

/*
 * send invite email
 */
exports.invite = function(req, res, next) {
  var me_id = req.query.userId;
  User.findOne({_id: me_id}, function(err, me) { 
    if (err) return next(err);
    User.findOne({email : req.query.email},  function (err, user) {
      if(err) return res.send(500, err);
      if (!user){
        var inv_instans = new Invite();
        inv_instans.user = me;
        inv_instans.save(function(err) {
          if (err) next( err);
          var params = {
            from:    "no reply to this email", 
            to:      "user <"+req.query.email+">",
            subject: "invite to editor pofile",
            attachment: [{
              data:'<html>To sign up just click to: <a href="' + 
                config.domain +'/signup/' + inv_instans.inviteId 
                + '">Users</a></html>',
              alternative:true
            }]
          };
          sendEmail(params);
        });
      }else{
        var relation = new UserEditor();
        relation.user = me;
        relation.editor = user;
        relation.save(function(err) {
          if (err) next(err);
        });
      }
    });
  });
  res.json(200, {status: 'success'});
};