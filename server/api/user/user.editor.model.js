'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserEditorSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  editor: {
    type: Schema.ObjectId,
    ref: 'User'
  },
});
  
module.exports = mongoose.model('UserEditor', UserEditorSchema);
