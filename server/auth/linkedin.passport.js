'use strict';

var passport = require('passport');
var User = require('../api/user/user.model');
var express = require('express');
var auth = require('./auth.service');
var router = express.Router();
var config = require('../config/environment');
var LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;


passport.use(new LinkedInStrategy({
  clientID: config.linkedin.clientID,
  clientSecret: config.linkedin.clientSecret,
  callbackURL: "http://test.pasport.com:9000/auth/linkedin/callback",
  scope: ['r_emailaddress', 'r_fullprofile'],
}, function(accessToken, refreshToken, profile, done) {    
  User.findOne({
    'linkedin_id': profile.id
  }, function(err, user) {
    if (!user) {
      user = new User({
        'linkedin_id' : profile.id,
        name: profile.displayName,
        role: 'user',
        email: profile._json.emailAddress,
        provider: 'linkedin',
        educations: profile._json.educations,
        skills: profile._json.skills,
        positions: profile._json.positions
       // linkedin: profile
      });
      user.save(function(err) {
        if (err)
          done(err);
        return done(err, user);
      });
    } else {
      return done(err, user);
    }
  });
}));

router.get('/',
  passport.authenticate('linkedin', { state: 'SOME STATE'  }),
  function(req, res){
    // The request will be redirected to LinkedIn for authentication, so this
    // function will not be called.
  });
  
router.get('/callback', passport.authenticate('linkedin', {
 // successRedirect: '/3333',
  failureRedirect: '/login',
  session: false
}), auth.setTokenCookie );

module.exports = router;