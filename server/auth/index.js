'use strict';

var express = require('express');
var router = express.Router();

router.use('/local', require('./local.passport'));
router.use('/linkedin', require('./linkedin.passport'));

module.exports = router;