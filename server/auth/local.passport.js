'use strict';

var express = require('express');
var passport = require('passport');
var auth = require('./auth.service');
var router = express.Router();
var User = require('../api/user/user.model');
var LocalStrategy = require('passport-local').Strategy;

function strategyCallback(email, password, done) {
  User.findOne({
    email: email.toLowerCase()
  }, function(err, user) {
    if (err)
      return done(err);

    if (!user) {
      return done(null, false, {message: 'This email is not registered.'});
    }
    if (!user.authenticate(password)) {
      return done(null, false, {message: 'This password is not correct.'});
    }
    return done(null, user);
  });
}

var strategy = new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password' // this is the virtual field on the model
}, strategyCallback);

passport.use(strategy);

/**
 * @api {post} /auth/local Login with email/password
 * @apiName LocalAuth
 * @apiGroup Auth
 *
 * @apiParam {String} email Email
 * @apiParam {String} password Password
 *
 * @apiSuccess {String} status Request status
 * @apiSuccess {String} data Authorization token
 *
 * @apiSuccessExample {json} Success-Response:
 * {
 *   "status": "success",
 *   "data": "..."
 * }
 *
 * @apiSampleRequest /auth/local
 *
 * @apiVersion 0.0.1
 */
router.post('/', function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) {
      return helpers.status.error(res, err);
    }
    if (info) {
      return helpers.status.badRequest(res, info.message);
    }
    if (!user) {
      return helpers.status.notFounded(res, 'Something went wrong, please try again.');
    }

    var token = auth.signToken(user._id, user.role);
    res.json({token: token});
  })(req, res, next);
});

module.exports = router;