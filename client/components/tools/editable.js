'use strict';

angular.module('usereditApp').directive('editInPlace', ['$timeout', function( $timeout) {
    return {
        restrict: 'E',
        scope: {
            value: '=',
            chenge_fn: '=chenge',
            changevalue:'=onchangevalue'
        },
        template: '<span ng-click="edit()" ng-bind="value"></span><input ng-change="chenge_loc()" class="c_editable editable-has-buttons editable-input form-control" ng-model="value"></input>',
        link: function ($scope, element, attrs) {
            // Let's get a reference to the input element, as we'll want to reference it.
            var inputElement = angular.element(element.children()[1]);

            // ng-click handler to activate edit-in-place
            $scope.chenge_loc = function () { 
                if( typeof($scope.chenge_fn) === 'undefined' ){
                  return;
                }
                if( typeof($scope.changevalue) === 'undefined' ){
                  $scope.chenge_fn();
                  return;
                }
                $timeout(function (){
                  $scope.chenge_fn($scope.changevalue);    
                }, 1);    
            };

            // This directive should have a set class so we can style it.
            element.addClass('edit-in-place');

            // Initially, we're not editing.
            $scope.editing = false;

            // ng-click handler to activate edit-in-place
            $scope.edit = function () {
                $scope.editing = true;

                // We control display through a class on the directive itself. See the CSS.
                element.addClass('active');

                // And we must focus the element. 
                // `angular.element()` provides a chainable array, like jQuery so to access a native DOM function, 
                // we have to reference the first element in the array.
                inputElement[0].focus();
            };

            // When we leave the input, we're done editing.
            inputElement.on('blur', function () {              
                $scope.editing = false;
                element.removeClass('active');
            }); 
        }
    };
}]);