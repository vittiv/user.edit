'use strict';

angular.module('usereditApp')
  .factory('User', function ($resource) {
    return $resource('/api/users/:id/:controller', {
      id: '@_id'
    },
    {
      changePassword: {
        method: 'PUT',
        params: {
          controller:'password'
        }
      },
      get: {
        method: 'GET',
        params: {
          id:'me'
        }
      },
      getEditableUsers: {
        method: 'GET',
        params: {
          controller:'editable_users'
        }
      }
	  });
  });
