'use strict';

angular.module('usereditApp')
  .controller('SignupCtrl', function ($scope, Auth, $location, $stateParams) {
        
    $scope.user = {};
    $scope.errors = {};
    
    $scope.register = function(form) {
      $scope.submitted = true;

      if(form.$valid) {
        
        var user = {
          name: $scope.user.name,
          email: $scope.user.email,
          password: $scope.user.password
        };
        if($stateParams.inviteId !== undefined){    
          user.invite = $stateParams.inviteId;  
        }

        Auth.createUser(user).then( function() {
          // Account created, redirect to home
          $location.path('/');
        })
        .catch( function(err) {
          err = err.data;
          $scope.errors = {};

          // Update validity of form fields that match the mongoose errors
          angular.forEach(err.errors, function(error, field) {
            form[field].$setValidity('mongoose', false);
            $scope.errors[field] = error.message;
          });
        });
      }
    };

  });
