'use strict';

angular.module('usereditApp')
  .controller('UsersCtrl', function ($scope, $http, User, socket, Modal, Auth) {
           
    var CurrentUserId = Auth.getCurrentUser()._id;
      
    var emailModal;
  
    $scope.action = function() {
      emailModal = Modal.openModal({
        modal: {
          email : '',
          dismissable: true,
          title: 'Please, provide email address',
          buttons: [{
            classes: 'btn-primary',
            text: 'Add',
            click: function(e) {
              $scope.mail(emailModal.scope.modal.email);
              emailModal.fn.close(e);
            }
          }, {
            classes: 'btn-default',
            text: 'Cancel',
            click: function(e) {
              emailModal.fn.dismiss(e);
            }
          }]
        }
      }, 'modal-primary', 'components/modal/email.html');
    };
    
    $scope.mail = function(email) {    
      if(email){
        $http.get('/api/users/invite',
                  {params: {email: email, userId: CurrentUserId }}
        ).success(function(data){});
      }
    };
    
    function setUsers() {
      $scope.users = User.getEditableUsers({ id: CurrentUserId }); 
    }
    setUsers();
   
    $scope.edit_close = function() {
      $scope.formShow = false;
    };

    $scope.edit = function(user) {
      $scope.formShow = true;
      $scope.user = user;
    };
    
    $scope.chenge = function(user) {  
      socket.socket.emit('user.update', user);
    };
    
    socket.socket.on('user.changed', function(data){  
      setUsers();
      $scope.user = data;       
    });

});
